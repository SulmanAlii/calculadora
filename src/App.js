import './App.css';
import Calculadora from './Calculadora';

function App() {
  return (
    <div className="App">
    <h1 style={{fontSize:"4rem"}}>REACT CALCULATOR</h1>
      <Calculadora />
    </div>
  );
}

export default App;
