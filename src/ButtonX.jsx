import React, { useState } from 'react'
import {Button} from 'reactstrap';

import './button.css';
import Display from './Display';


const ButtonX = props => {

    return (
        <div className="main_botons">      
                    <Button  onClick={() => props.clear()} id="clear">CLEAR</Button>
                    <Button value="0" onClick={(e) => props.onClick(e.target.value)}>0</Button>
                    <Button onClick={() => props.deleteNumber()} id="C">C</Button>
                    <Button value="-" onClick={(e) => props.onClick(e.target.value)}>-</Button>
                    <Button value="1" onClick={(e) => props.onClick(e.target.value)}>1</Button>
                    <Button value="2" onClick={(e) => props.onClick(e.target.value)}>2</Button>
                    <Button value="3" onClick={(e) => props.onClick(e.target.value)}>3</Button>
                    <Button value="+" onClick={(e) => props.onClick(e.target.value)}>+</Button>
                    <Button value="4" onClick={(e) => props.onClick(e.target.value)}>4</Button>
                    <Button value="5" onClick={(e) => props.onClick(e.target.value)}>5</Button>
                    <Button value="6" onClick={(e) => props.onClick(e.target.value)}>6</Button>
                    <Button value="/" onClick={(e) => props.onClick(e.target.value)}>/</Button>
                    <Button value="8" onClick={(e) => props.onClick(e.target.value)}>8</Button>
                    <Button value="9" onClick={(e) => props.onClick(e.target.value)}>9</Button>
                    <Button value="7" onClick={(e) => props.onClick(e.target.value)}>7</Button>
                    <Button value="*" onClick={(e) => props.onClick(e.target.value)}>x</Button>
                    <Button  id="result" onClick={(e) => props.Resultado()}>RESULT</Button>
        </div>
    )
}


export default ButtonX;
