import React, { useState } from "react";
import ButtonX from "./ButtonX";
import "./calculadora.css";
import Display from "./Display";

const Calculadora = () => {

    const [display, setDisplay] = useState();
    const [Value, setValue] = useState("");
    const [resultado, setResultado] = useState(" ");

const PulsaBoton = (x) =>{
    setValue(Value.concat(x));    
}

const Resultado = () => {
  try{
  setValue(eval(Value).toString());
  }catch(Error){
    setValue("ERROR");
  }
}

const clear = () =>{
  setValue(" ");
}

const deleteNumber = () =>{
  setValue(Value.slice(0, Value.length - 1));
}


  return (
    <div className="calculator">
        <div className="calculator_grid">
        <div className="display">
            <Display showNumber={Value}/>
        </div>
        <ButtonX onClick={PulsaBoton} Resultado={Resultado} clear={clear} deleteNumber={deleteNumber}></ButtonX>
      </div>
    </div>
  );
};

export default Calculadora;
